var CurrentID = -1;
// listController
var listController = (function(){
  var ListItem = function(id, description){
    this.id = id;
    this.description = description;
  };
  var data = {
      des:[],
      checked:[]
  };

  var ObjIndex = function(id){
    var ids;
    ids = data.des.map(function(current){
      return current.id;
    });
    index = ids.indexOf(id);
    return index;
  };

  return {
    addItem: function(des){
      var newItem, ID;
      // Create a new ID
      if (data.des.length > 0) {
          // it will take the last id add continue from there
          ID = (data.des[data.des.length - 1].id) + 1;
      } else {
          ID = 0;
      }
      newItem = new ListItem(ID, des);
      // Push the new element to our structure
      data.des.push(newItem);

      // return the new elment
      return newItem;
    },
    deleteItem:function(id){
      var index = ObjIndex(id);
      if(index !== -1){
        data.des.splice(index,1);
      }
    },

    setItemDescreption: function(id, description){
      var index = ObjIndex(id);
      if(index != -1){
        data.des[index].description = description;
      }
    },

    getItem: function(itemID){
      var splitId, info = [];
      if(itemID){
        splitId = itemID.split('-');
        info[0] = (splitId[0]);
        info[1] = parseInt(splitId[1]);
      }
      return info;
    },
    getDataById: function(id){
      var index = ObjIndex(id);
      if(index !== -1){
        return data.des[index];
      }
    },
    addShoppingItems: function(id, values){
      // check all input fields
      // add all values to data var
      if(values){
        for(var i = 0; i < values.length; i++){
          var e = values[i];
          data.des[id][e.name] = e.value;
        }
      }
    },
    testing: function(){
      console.log(data);
    }
  };

})();

// listUI
var listUI = (function(){
  var DOMstrings = {
    container: '.container',
    shoppingList: '.shopping-list',
    addItem: '.add-item',
    removeItem: '#remove-item'
  };
  return {
    addListItem: function(obj){
      var html, newHtml, element;
      // 1. create html string with place holder text
      element = DOMstrings.shoppingList;

      html = '<div id="list-%id%" class="under-line"><div class="list-padding"><input id="list-check" class="" type="checkbox" name="" id=""><label id="item-des-%id%" class="list-padding" for="" contenteditable="true">Add your Item Here</label><div id="remove-item" class="list-remove close">X</div></div></div>';

      // 2. Replace the place holder text with some actual data
      newHtml = html.replace('%id%', obj.id);
      newHtml = newHtml.replace('%id%', obj.id);
      // 3. Insert the html to the DOM
      document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
    },
    closeItem: function(selectorID){
      var el;
      el = document.getElementById(selectorID);
      el.parentNode.removeChild(el);

    },
    showShoppingDes: function(selectorID, data){
      var el = document.getElementById('shopping-des-con');
      el.getElementsByClassName('title')[0].childNodes[0].textContent = data.description;

      var values = (Object.keys(data).map(key => data[key]));
      if(values.length > 2){
        for(var i = 0; i < values.length - 2; i++){
          el.childNodes[1].childNodes[1].elements[i].value = values[i + 2];
        }
      }else{
        document.getElementById("listForm").reset();
      }

      el.classList.remove('des-list');
    },

    removeShoppingDes: function(){
      var el = document.getElementById('shopping-des-con');
      el.classList.add('des-list');
    },
    getDomStrings: function(){
      return DOMstrings;
    }
  };

})();

// Global App Controller
var controller = (function(listCtrl, UICtrl){

  var SetupEventListeners = function() {
    var DOM = UICtrl.getDomStrings();
    document.querySelector(DOM.addItem).addEventListener('click', CtrlAddItem);
    document.querySelector(DOM.container).addEventListener('click', CtrlDeleteItem);
    document.querySelector(DOM.container).addEventListener('click', CtrlShowListdes);
    document.querySelector(DOM.container).addEventListener('focusout', CtrlRemoveShoppingDes);
    document.querySelector(DOM.container).addEventListener('click', CtrlCheckedItem);
    document.querySelector(DOM.container).addEventListener('focusout', CtrlItemDesc);
    document.querySelector(DOM.container).addEventListener('focusout', CtrlAddshoppingItems);
  };
  //
  var CtrlShowListdes = function(event){
    var itemID, ID, data;

    if(event.target.parentNode.id.includes('list')){

      // Get the ID of the Item
      itemID = event.target.parentNode.id;
      itemID = listCtrl.getItem(itemID);
      ID = itemID[1];

      data = listCtrl.getDataById(ID);

      // Show the shopping List -- UIcontroller
      UICtrl.showShoppingDes(ID, data);
      // Add the content using the ID
      CurrentID = ID;
      listCtrl.addShoppingItems(ID);

    }
  };
  // Remove shopping list properties if press out or press another
  var CtrlRemoveShoppingDes =function(event){


  };
  var CtrlAddshoppingItems = function(event){
    var el, values;
    el = event.target.parentNode.parentNode.parentNode.parentNode;

    if(el.id === 'shopping-des-con'){

      values = el.childNodes[1].childNodes[1].elements;
      listCtrl.addShoppingItems(CurrentID, values);

    }

  };

  // When cahnging the item descreption
  var CtrlItemDesc = function(event){
    var itemID;

    if(event.target.tagName === 'LABEL'){
      var itemDescreption;
      itemID = event.target.parentNode.parentNode.id;

      itemID = listCtrl.getItem(itemID);

      // save the new descreption
      itemDescreption = document.getElementById('item-des-' + itemID[1]).textContent;
      listCtrl.setItemDescreption(itemID[1], itemDescreption);
    }
  };
  var CtrlCheckedItem = function(event){
    var itemID;
    // Add class checked to the label
    if(event.target.id === 'list-check'){
      event.target.parentNode.childNodes[1].classList.toggle('checked');
    }

  };
  var CtrlAddItem = function(event) {

    if(event.target.classList.value === 'add-item'){
      UICtrl.removeShoppingDes();
      // Add the item to the UI
      var item = listController.addItem('Add your Item Here');
      //listCtrl.addListItem('obj', 'type');
      listUI.addListItem(item);
    }
  };
  var CtrlDeleteItem = function(event){
    var itemID, splitID, ID, info;

    if(event.target.id === 'remove-item'){
      UICtrl.removeShoppingDes();
      itemID = event.target.parentNode.parentNode.id;
      if(itemID){
        info = listCtrl.getItem(itemID);
        ID = info[1];

        listCtrl.deleteItem(ID);
        listUI.closeItem(itemID);
      }
    }
  };
  return {
    init: function(){
      console.log('Applecation Started');
      SetupEventListeners();
    }
  };

})(listController,listUI);

controller.init();
